# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase
import datetime
import random
random.seed()

from hamcrest import assert_that

from commodity.mail import (GmailSmtpAccount, GmailImapAccount,
                            SMTP, IMAP, meets_imap_query)


class TestSMTP(TestCase):
    def test_sendmail(self):
        try:
            password = file('PASS').read().strip()
        except IOError as e:
            self.skipTest(str(e))

        fromaddr = 'arco.logging@gmail.com'

        smtp_account = GmailSmtpAccount(
            username = fromaddr,
            password = password)

        key = str(random.randint(1, 100000))

        smtp = SMTP(smtp_account, fromaddr, ssl=True)
        smtp.sendmail(fromaddr, subject=key, body="")

        imap_account = GmailImapAccount(
            username = fromaddr,
            password = password)

        imap = IMAP(imap_account)
        assert_that(imap, meets_imap_query('(SUBJECT %s)' % key))


class TestIMAP(TestCase):
    def test_create_imap(self):
        try:
            password = file('PASS').read().strip()
        except IOError as e:
            self.skipTest(str(e))

        account = GmailImapAccount(
            username = 'arco.logging@gmail.com',
            password = password)

        sut = IMAP(account)

#        for m in sut.messages():
#            print '-', m['Subject'][:100]

        print 'TODAY'
        today = datetime.datetime(2013, 2, 17)
        for m in sut.messages('(SUBJECT "{0:%Y.%m.%d}")'.format(today)):
            print '-', m['Subject'][:100]

        assert_that(sut, meets_imap_query('(SUBJECT {0:%Y.%m.%d})'.format(today)))
        assert_that(sut, meets_imap_query('(SENTON {0:%d-%b-%Y})'.format(today)))
        assert_that(sut, meets_imap_query('(ON {0:%d-%b-%Y})'.format(today)))
        assert_that(sut, meets_imap_query('(ON {0:%d-%b-%Y} SUBJECT X2)'.format(today)))
