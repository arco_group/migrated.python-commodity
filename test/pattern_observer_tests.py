# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase

from doublex import Spy, Mock, method_raising, verify, assert_that, called

from commodity.pattern import Observable


class SomeException(Exception):
    pass


class TestObservable(TestCase):

    def test_attached_observer_is_notified(self):
        # given
        sut = Observable()
        observer = Spy()
        sut.attach(observer.update)

        # when
        sut.notify(4)

        # then
        assert_that(observer.update, called().with_args(4))

    def test_invalid_observer(self):
        # given
        sut = Observable()
        observer = None

        # when/then
        with self.assertRaises(Observable.InvalidObserver):
            sut.attach(observer)

    def test_exception_on_notify_observer(self):
        # given
        observer = method_raising(SomeException)
        sut = Observable()
        sut.attach(observer)

        # when/then
        with self.assertRaises(Observable.ObserverException) as cm:
            sut.notify(4)

        self.assertEquals(cm.exception.args[0].__class__, SomeException)

    def test_detached_observer_not_updated(self):
        # given
        sut = Observable()
        observer = Mock()
        observer.update = lambda x: None
        sut.attach(observer.update)
        sut.detach(observer.update)

        # when
        sut.notify(4)

        # then
        assert_that(observer, verify())
