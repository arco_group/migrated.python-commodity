# -*- mode:python; coding:utf-8 -*-

import sys
import math
import time
import threading
from unittest import TestCase

sys.path.insert(0, '.')

from commodity.thread_ import SimpleThreadPool


class TestSimplePool(TestCase):
    def setUp(self):
        self.pool = SimpleThreadPool(3)

    def square(self, x):
        time.sleep(float(x) / 8)
        return x * x

    def test_func_without_args(self):
        store = []
        self.pool.add(str, callback=store.append)
        self.pool.join()

        self.assertEquals(store[0], '')

    def test_no_callback(self):
        class Counter:
            def __init__(self):
                self.value = 0
                self.lock = threading.Lock()

            def inc(self):
                with self.lock:
                    self.value += 1

        counter = Counter()
        for i in range(3):
            self.pool.add(counter.inc)
        self.pool.join()

        self.assertEquals(counter.value, 3)

    def test_square_by_add(self):
        results = []

        for i in range(10):
            self.pool.add(self.square, (i,), callback=results.append)
        self.pool.join()

        self.assertEquals(
            sorted(results),
            [0, 1, 4, 9, 16, 25, 36, 49, 64, 81])

    def test_square_by_map(self):
        results = self.pool.map(self.square, range(9, -1, -1))

        self.assertEquals(
            results, (81, 64, 49, 36, 25, 16, 9, 4, 1, 0))

    def test_pow_by_map(self):
        results = self.pool.map(math.pow, [2, 3, 3], [2, 2, 4])

        self.assertEquals(
            results, (4, 9, 81))
