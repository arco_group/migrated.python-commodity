#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
from unittest import TestCase

from commodity import path
curdir = os.getcwd()
TOKEN = 'tokenfile'


class PathTest(TestCase):
    def setUp(self):
        os.system('touch %s' % os.path.join(curdir, TOKEN))

    def test_find_in_ancestors_ok(self):
        fpath = os.path.join(curdir, 'test/util/print.py')
        self.assertEqual(path.find_in_ancestors(TOKEN, fpath), curdir)

    def test_find_in_ancestors_fail(self):
        self.assertEqual(path.find_in_ancestors(TOKEN, '/usr/share/doc'), None)

    def test_get_project_dir(self):
        project_dir = path.get_project_dir(__file__)
        print(project_dir)
        self.assertTrue(project_dir.endswith('python-commodity'))
