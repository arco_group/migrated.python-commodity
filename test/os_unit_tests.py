# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase
import functools
import io

from commodity.os_ import SubProcess, wait_or_raise, FuncAsTextFile
from commodity.net import is_port_open


class wait_or_raise_tests(TestCase):
    def test_wait_server_ready__ok(self):
        assert not is_port_open(2000)
        server = SubProcess('ncat -l 2000')
        wait_or_raise(functools.partial(is_port_open, 2000),
                      delta=0.5)
        server.terminate()

    def test_wait_server_ready__fail(self):
        assert not is_port_open(2000)
        assert not is_port_open(5000)
        server = SubProcess('ncat -l 2000')

        with self.assertRaises(AssertionError):
            wait_or_raise(functools.partial(is_port_open, 5000),
                          delta=0.5, timeout=1)

        server.terminate()

    def test_traceback(self):
        def f():
            return False

        wait_or_raise(f, timeout=2)


#    def test_wait_server_become_ready(self):
#        assert_that(localhost, is_not(listen_port(2000)))
#        server = SubProcess('ncat -l 2000')
#        wait_that(localhost, listen_port(2000))
#        server.terminate()


class FuncAsTextFile_tests(TestCase):
    def test_using_TextIOWrapper(self):
        io.TextIOWrapper(FuncAsTextFile(lambda x: x))
