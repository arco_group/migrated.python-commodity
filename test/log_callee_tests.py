# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase

from commodity.log import CallerData

from hamcrest import assert_that, is_


class CallerDataTests(TestCase):
    def test_depth_0(self):
        def a():
            return b()

        def b():
            return CallerData()

        assert_that(a().statement, is_('return b()'))

    def test_depth_1(self):
        def a():
            return b()

        def b():
            return c()

        def c():
            return CallerData(1)

        assert_that(a().statement, is_('return b()'))
