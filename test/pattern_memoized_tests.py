# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
from unittest import TestCase, skipIf
import itertools
import os
import random
random.seed(os.getpid())

from doublex import assert_that, Spy, called

from commodity.pattern import memoizedproperty


if sys.version_info < (3, 2):
    from .pattern import memoized
elif sys.version_info < (3, 9):
    from functools import lru_cache as memoized
else:
    from functools import cache as memoized


class Some(object):
    @memoized
    def method(self, arg):
        return random.randrange(1000)

    @memoized
    def method2(self, a, b):
        return random.randrange(1000)

    # @memoized(ignore=['b'])
    # def method2_ignore(self, a, b):
    #     return random.randrange(1000)

    # @memoized(ignore=['c'])
    # def wrong_ignore(self, a, b):
    #     return random.randrange(1000)

    @classmethod
    @memoized
    def class_method(cls, arg):
        return random.randrange(1000)

    @staticmethod
    @memoized
    def static_method(arg):
        return random.randrange(1000)

    @memoizedproperty
    def property_method(self):
        return random.randrange(1000)


@skipIf(sys.version_info > (3, 2), 'Python 3 has functools.lru_cache amd functools.cache')
class MemoizedTests(TestCase):
    def test_func_one_arg(self):
        @memoized
        def func(arg):
            return random.randrange(1000)

        self.assertEquals(func(1), func(1))
        self.assertNotEquals(func(1), func(2))

    def test_func_two_args(self):
        @memoized
        def func2(a, b):
            return random.randrange(1000)

        self.assertEquals(func2(5, 6),
                          func2(5, 6))
        self.assertNotEquals(func2(5, 6),
                             func2(8, 9))

    # def test_func_two_args_ignore(self):
    #     @memoized(ignore=['b'])
    #     def func2_ignore(a, b):
    #         return random.randrange(1000)

    #     self.assertEquals(func2_ignore(10, 12),
    #                       func2_ignore(10, 200))
    #     self.assertNotEquals(func2_ignore(10, 12),
    #                          func2_ignore(99, 12))

    def test_bound_method_one_arg(self):
        some1 = Some()
        some2 = Some()

        self.assertEquals(some1.method(2), some1.method(2))
        self.assertNotEquals(some1.method(2), some2.method(2))
        self.assertEquals(some2.method(2), some2.method(2))

    def test_bound_method_two_args(self):
        some = Some()
        self.assertEquals(some.method2(2, 3), some.method2(2, 3))

    def test_classmethod_one_arg(self):
        self.assertEquals(Some.class_method(2),
                          Some.class_method(2))

    def test_staticmethod_one_arg(self):
        self.assertEquals(Some.static_method(2),
                          Some.static_method(2))

    def test_bound_method2_ignore(self):
        some = Some()

        self.assertEquals(some.method2_ignore(2, 3),
                          some.method2_ignore(2, 100))

    def test_ignore_with_missing_argument(self):
        some = Some()

        with self.assertRaises(ValueError):
            some.wrong_ignore(1, 2)


class TestMemoizedProperty(TestCase):
    def test_property(self):
        some = Some()

        self.assertEquals(some.property_method, some.property_method)

    def test_different_instances(self):
        some1 = Some()
        some2 = Some()

        self.assertEquals(some1.property_method, some1.property_method)
        self.assertNotEquals(some1.property_method, some2.property_method)
        self.assertEquals(some2.property_method, some2.property_method)
