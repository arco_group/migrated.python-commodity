import unittest

from commodity.deco import suppress_errors
from doublex import method_raising, method_returning, Mock, assert_that, verify


class ExceptionTest(unittest.TestCase):
    def test_creation(self):
        def foo():
            return False
        sut = suppress_errors(foo)

        self.assertIsInstance(sut, type(foo))
        self.assertEquals(sut.__name__, foo.__name__)
        self.assertEquals(sut.__module__, foo.__module__)

    def test_basic_exceptions_are_not_launched(self):
        obj = object()
        foo = method_returning(obj)

        sut = suppress_errors(foo)

        self.assertEquals(obj, sut())

    def test_default_value(self):
        obj = object()
        foo = method_raising(Exception)

        sut = suppress_errors(foo, default=obj)

        self.assertEquals(obj, sut())

    def test_exception_is_launched_if_avoided(self):
        foo = method_raising(Exception)

        with self.assertRaises(Exception):
            sut = suppress_errors(foo, errors=[NotImplementedError])
            sut()

    def test_exception_is_ignored_when_explicit(self):
        foo = method_raising(NotImplementedError)

        sut = suppress_errors(foo, errors=[NotImplementedError])

        self.assertEqual(None, sut())

    def test_exceptions_are_logged(self):
        foo = method_raising(NotImplementedError)
        mock = Mock()
        mock.logger = lambda x: False

        sut = suppress_errors(foo, errors=[NotImplementedError], log_func=mock.logger)

        self.assertEqual(None, sut())
        assert_that(mock, verify())


class ExceptionsAsDecoratorTests(unittest.TestCase):
    def test_creation(self):
        @suppress_errors
        def sut():
            return False

        self.assertIsInstance(sut, type(sut))
        self.assertEquals(sut.__name__, sut.__name__)
        self.assertEquals(sut.__module__, sut.__module__)

    def test_default_value(self):
        @suppress_errors(default=1)
        def sut():
            raise Exception()

        self.assertEquals(1, sut())

    def test_exceptions_are_logged(self):
        mock = Mock()
        mock.logger = lambda x: False
        @suppress_errors()
        def sut(log_func=mock.logger):
            raise Exception()

        self.assertEqual(None, sut())
        assert_that(mock, verify())
