# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase

from doublex import Spy, assert_that, called

from commodity.os_ import LineStreamPrefixer


class Test_LineStreamPrefixer(TestCase):
    def test_1_line(self):
        sink = Spy()
        sut = LineStreamPrefixer(sink, 'pre| ')

        sut.write('line 1')

        assert_that(sink.write, called().with_args('pre| line 1'))

    def test_2_lines(self):
        sink = Spy()
        sut = LineStreamPrefixer(sink, 'pre| ')

        sut.write('line 1\nline 2')

        assert_that(sink.write, called().with_args('pre| line 1\npre| line 2'))

    def test_trailing_newline(self):
        sink = Spy()
        sut = LineStreamPrefixer(sink, 'pre| ')

        sut.write('line 1\n')

        assert_that(sink.write, called().with_args('pre| line 1\n'))

    def test_2_writes_without_newline(self):
        sink = Spy()
        sut = LineStreamPrefixer(sink, 'pre| ')

        sut.write('line 1')
        sut.write('line 2')

        assert_that(sink.write, called().with_args('pre| line 1'))
        assert_that(sink.write, called().with_args('line 2'))
