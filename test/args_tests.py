# -*- coding:utf-8; tab-width:4; mode:python -*-

import argparse
from unittest import TestCase

import commodity.args

try:
    reload
except NameError:
    from imp import reload


class ArgsTests(TestCase):
    def setUp(self):
        reload(commodity.args)
        global ArgumentConfigParser
        self.sut = commodity.args.parser

    def test_positional(self):
        self.sut.add_argument("name", type=str)

        args = self.sut.parse_args(["john"])

        self.assertEquals(args.name, "john")

    def test_builtin_bevabior(self):
        self.sut.add_argument('--foo', '-f', type=int)
        args = self.sut.parse_args("-f 3".split())

        self.assertEquals(args.foo, 3)

    def test_absent_argument_without_default_is_none(self):
        self.sut.add_argument('--foo', '-f', type=int)
        args = self.sut.parse_args([])

        self.assertEquals(args.foo, None)

    def test_default(self):
        self.sut.add_argument('--foo', '-f', type=int, default=5)

        args = self.sut.parse_args([])

        self.assertEquals(args.foo, 5)

    def test_store_const_set_default(self):
        self.sut.add_argument('--http', dest='proto', action='store_const', const='http', default='http')
        self.sut.add_argument('--ssh', dest='proto', action='store_const', const='ssh')

        args = self.sut.parse_args([])

        self.assertEquals('http', args.proto)

    def test_store_const_set_with_cmdline(self):
        self.sut.add_argument('--http', dest='proto', action='store_const', const='http', default='http')
        self.sut.add_argument('--ssh', dest='proto', action='store_const', const='ssh')

        args = self.sut.parse_args(['--ssh'])

        self.assertEquals('ssh', args.proto)

    def test_remainder(self):
        self.sut.add_argument('--foo')
        self.sut.add_argument('command')
        self.sut.add_argument('args', nargs=argparse.REMAINDER)
        result = self.sut.parse_args('--foo B cmd --arg1 XX ZZ'.split())

        self.assertEquals(['--arg1', 'XX', 'ZZ'], result.args)

    def test_store_const_set_with_config(self):
        CONFIG = '''\
[ui]
proto = ssh
'''.splitlines()

        self.sut.add_argument('--http', dest='proto', action='store_const', const='http')
        self.sut.add_argument('--ssh', dest='proto', action='store_const', const='ssh')
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args([])

        self.assertEquals('ssh', args.proto)

    def test_load_config(self):
        CONFIG = '''\
[ui]
foo = 73
'''.splitlines()

        self.sut.add_argument('--foo', '-f', type=int)
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args([])

        self.assertEquals(args.foo, 73)

    def test_load_config_with_incompatible_type_raises_exception(self):
        CONFIG = '''\
[ui]
foo = bar
'''.splitlines()

        self.sut.add_argument('--foo', '-f', type=int)
        self.sut.load_config(CONFIG)

        with self.assertRaises(ValueError):
            self.sut.parse_args([])

    def test_text_argument(self):
        CONFIG = '''\
[ui]
account = john.doe:secret
'''.splitlines()

        self.sut.add_argument('--account', '-a', type=str)
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args([])

        self.assertEquals(args.account, 'john.doe:secret')

    def test_subparser_positional(self):
        subparsers = self.sut.add_subparsers(dest="subcmd")
        list_parser = subparsers.add_parser("list")
        list_parser.add_argument("user")

        self.sut.parse_args("list john".split())

    def test_subsection_argument_chosen_by_key(self):
        CONFIG = '''\
[ui]
  [[account]]
  john = john.doe:secret
'''.splitlines()

        self.sut.add_argument('--account', '-a', type=str)
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args('--account john'.split())

        self.assertEquals(args.account, 'john.doe:secret')

    def test_subsection_argument_choose_default(self):
        CONFIG = '''\
[ui]
  [[account]]
  default = foo.bar:buzz
  john = john.doe:secret
'''.splitlines()

        self.sut.add_argument('--account', '-a', type=str)
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args([])

        self.assertEquals('foo.bar:buzz', args.account)

    def test_subsection_argument_choose_inline(self):
        CONFIG = '''\
[ui]
  [[account]]
  default = foo.bar:buzz
  john = john.doe:secret
'''.splitlines()

        self.sut.add_argument('--account', '-a', type=str)
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args('-a other'.split())

        self.assertEquals('other', args.account)

    def test_load_argument_of_subcommand(self):
        CONFIG = '''\
[ui]
  [[cmd:clone]]
  destdir = /tmp/share
'''.splitlines()

        commands = self.sut.add_subparsers(dest='cmd')
        clone = commands.add_parser('clone')
        clone.add_argument('--destdir', dest='clone_destdir')
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args(['clone'])

        self.assertEquals('/tmp/share', args.clone_destdir)

    def test_load_argument_of_subcommand_inline_has_priority(self):
        CONFIG = '''\
[ui]
  [[cmd:clone]]
  destdir = /tmp/share
'''.splitlines()

        commands = self.sut.add_subparsers(dest='cmd')
        clone = commands.add_parser('clone')
        clone.add_argument('--destdir', dest='clone_destdir')
        self.sut.load_config(CONFIG)

        args = self.sut.parse_args('clone --destdir /var/repos'.split())

        self.assertEquals('/var/repos', args.clone_destdir)

    def test_counter(self):
        self.sut.add_argument('--verbosity', action='count', default=0)
        args = self.sut.parse_args([])
        self.assertEquals(0, args.verbosity)

    def test_prego_cli(self):
        SPECS = '''\
[ui]
stdout = boolean(default=true)
stderr = boolean(default=true)
keep_going = boolean(default=false)
verbosity = integer(default=0)
color = boolean(default=false)
dirty = boolean(default=false)
config = string(default=None)
'''.splitlines()

        DEFAULT = '''\
[ui]
color = true
stdout = false
stderr = false
'''.splitlines()

        OTHER = '''\
[ui]
stderr = true
'''.splitlines()

        return

#        parser = ArgumentConfigParser(prog='prego', specs=SPECS)

        parser.add_argument('-c', '--config',
                            help='explicit config file')
        parser.add_argument('-k', '--keep-going', action='store_true',
                            help="continue even with failed assertion or tests")

        parser.add_argument('-d', '--dirty', action='store_true',
                            help="do not remove generated files")

        parser.add_argument('-o', '--stdout', action='store_true',
                            help='print tests stdout')
        parser.add_argument('-e', '--stderr', action='store_true',
                            help='print tests stderr')
        parser.add_argument('-v', '--verbose', dest='verbosity', action='count',
                            help='increase log verbosity')

        parser.add_argument('-p', '--plain', action='store_false', dest='color',
                            default=False,
                            help='avoid colors and styling in output')

        parser.add_argument('nose', nargs=argparse.REMAINDER)
        parser.load_config(DEFAULT)
        args = parser.parse_args(['-v'])

        self.assertEquals(args.stdout, False)
        self.assertEquals(args.stderr, False)
        self.assertEquals(args.color, True)
        self.assertEquals(args.verbosity, 1)
        self.assertEquals(args.dirty, False)

        parser.load_config(OTHER)

        self.assertEquals(args.stderr, True)
