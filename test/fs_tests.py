# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import unittest

from commodity.fs import FakeFS

# -- FIXME: issues --

# -- 1. --
# f = fs.open('f1', 'w')
# f.write(b'some')
# f.close()
#
# fs.open('f1').read() == b'some'
# fs.open('f1').read() == b''


class Test_FakeDirectory(unittest.TestCase):
    def setUp(self):
        self.sut = FakeFS()

    def assert_dirs(self, contents):
        for dname, dirs in contents:
            self.assertEquals(set(self.sut.ls(dname)), set(dirs))

    def test_ls_empty(self):
        self.assertEquals(self.sut.ls(), [])

    def test_mkdir_empty(self):
        with self.assertRaises(OSError):
            self.sut.mkdir('')

    def test_mkdir_simple(self):
        self.sut.mkdir('hello')
        self.assertEquals(self.sut.ls(), ['hello'])

        self.sut.mkdir('/bye')
        self.assertEquals(set(self.sut.ls()), set(['hello', 'bye']))

    def test_mkdir_already_exists(self):
        self.sut.mkdir('foo')
        with self.assertRaises(OSError):
            self.sut.mkdir('foo')

    def test_mkdir_already_exists_2_levels(self):
        self.sut.mkdir('foo/bar')
        with self.assertRaises(OSError):
            self.sut.mkdir('foo/bar')

    # FIXME
    def test_mkdir_multi(self):
        path = 'usr/local/bin'
        self.sut.mkdir(path)

        items = path.split(os.sep)
        self.assert_dirs([
            ('', ['usr']),
            ('usr', ['local']),
            ('usr/local', ['bin']),
            ('usr/local/bin', []),
            ])

        self.assertEquals(self.sut.ls(path), [])

    def test_mkdir__parents_two_calls(self):
        self.sut.mkdir('usr')
        self.sut.mkdir('usr/local')
        self.sut.mkdir('usr/share')

        self.assert_dirs([
            ('', ['usr']),
            ('usr', ['local', 'share']),
            ('usr/local', []),
            ])

    def test_serveral_dirs(self):
        dirs = ['foo', 'bar', 'poor']
        for d in dirs:
            self.sut.mkdir(d)

        self.assertEquals(set(self.sut.ls()), set(dirs))

    def test_exists_missing(self):
        self.assertFalse(self.sut.exists('missing'))

    def test_exists_missing_multi(self):
        self.assertFalse(self.sut.exists('missing/other'))

        self.sut.mkdir('foo')
        self.assertFalse(self.sut.exists('foo/missing'))

    def test_exists_dir(self):
        self.sut.mkdir('foo')
        self.assertTrue(self.sut.exists('foo'))

    def test_exists_dir_multi(self):
        self.sut.mkdir('foo/bar')
        self.assertTrue(self.sut.exists('foo/bar'))

    def test_rmdir_missing(self):
        with self.assertRaises(OSError):
            self.sut.rmdir('mssing')

    def test_rmdir(self):
        self.sut.mkdir('foo')
        self.sut.rmdir('foo')

    def test_rmdir_missing_level1(self):
        self.sut.mkdir('foo')

        with self.assertRaises(OSError):
            self.sut.rmdir('foo/mssing')

    def test_rmdir_level1(self):
        self.sut.mkdir('foo/bar')
        self.sut.rmdir('foo/bar')
        self.sut.rmdir('foo/')


class Test_FakeFile(unittest.TestCase):
    def setUp(self):
        self.sut = FakeFS()

    def create_file(self, fpath):
        with self.sut.open(fpath, 'w') as fd:
            fd.write(b'some text')

    def test_new_file_root(self):
        fpath = 'some_file'
        self.create_file(fpath)
        self.assert_(fpath in self.sut.ls())

    def test_new_file_level1(self):
        fpath = 'dir1/file1'
        self.sut.mkdir('dir1')
        self.create_file(fpath)

        self.assert_('file1' not in self.sut.ls())
        self.assert_('file1' in self.sut.ls('dir1'))

    def test_missing_file_root(self):
        with self.assertRaises(OSError):
            self.sut.open('wrong')

    def test_missing_file_level1(self):
        path = 'dir1/wrong'
        with self.assertRaises(OSError) as cm:
            self.sut.open(path)

        self.assertEquals(cm.exception.filename, path)

    def test_save_load(self):
        content = b'hello file'
        with self.sut.open('file1', 'w') as fd:
            fd.write(content)

        with self.sut.open('file1') as f:
            self.assertEquals(f.read(), content)

        # twice
        with self.sut.open('file1') as f:
            self.assertEquals(f.read(), content)

    def test_remove_missing(self):
        path = 'file1.txt'
        self.assertFalse(self.sut.exists(path))

        with self.assertRaises(OSError) as cm:
            self.sut.remove(path)

    def test_remove(self):
        path = 'file1.txt'
        self.create_file(path)
        self.assertTrue(self.sut.exists(path))

        self.sut.remove(path)
        self.assertFalse(self.sut.exists(path))

    def test_remove_missing_level1(self):
        path = 'foo/file1.txt'
        self.sut.mkdir('foo')
        self.assertFalse(self.sut.exists(path))

        with self.assertRaises(OSError) as cm:
            self.sut.remove(path)

    def test_remove_level1(self):
        path = 'foo/file1.txt'
        self.sut.mkdir('foo')

        self.create_file(path)
        self.assertTrue(self.sut.exists(path))

        self.sut.remove(path)
        self.assertFalse(self.sut.exists(path))

    def test_stat(self):
        path = 'file1.txt'
        self.create_file(path)

        self.assertEquals(self.sut.stat(path).st_size, 9)

    def test_stat_root(self):
        print(self.sut.stat(''))

    def test_stat_level1(self):
        path = 'foo/file1.txt'
        self.sut.mkdir('foo')

        self.create_file(path)
        self.assertTrue(self.sut.exists(path))
        self.assertEquals(self.sut.stat(path).st_size, 9)

    def test_move(self):
        path1 = '/file1'
        path2 = '/file2'
        self.create_file(path1)
        self.sut.move(path1, path2)

        self.assertFalse(self.sut.exists(path1))
        self.assertTrue(self.sut.exists(path2))

    def test_move_level1(self):
        path1 = 'foo/file1'
        path2 = 'bar/file2'

        self.sut.mkdir('foo')
        self.sut.mkdir('bar')

        self.create_file(path1)
        self.sut.move(path1, path2)

        self.assertFalse(self.sut.exists(path1))
        self.assertTrue(self.sut.exists(path2))

        self.assertEquals(self.sut.ls('bar'), ['file2'])
