0.20210223
==========

* os_.SupProcess match behabior for PIPE, STDOUT and DEVNULL values

0.20131227
==========

* Improving embedded documentation.
* Documentation now at readthedocs.org.
* Now deco.handle_exception handler receives the exception as argument.
* deco.memoized support 'ignore' for plain functions.
* Improving deco.memoized tests

0.20130711
==========

* deco.add_attr
* testing.wait_that default attributes

0.20130426
==========

* pypi project renamed to "commodity"


0.20130217
==========

* remove get_project_dir

0.20121121
==========

* os_.SubProcess accepts unicode command

0.20121016
==========

* LineStreamPrefixer needs a 'closed' attribute.

0.20121008
==========

* str_.Printable

0.20120903
==========

* os_.LoggerAsFile

0.20120418
==========

* ThreadFunc get 'result' and 'exception' atributes.
* str_.add_prefix


.. Local Variables:
..  coding: utf-8
..  mode: rst
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
