.. Commodity documentation master file, created by
   sphinx-quickstart on Wed Jan  9 19:02:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


================
Commodity's docs
================

``commodity`` is a library of recurrent required utilities for Python programmers.


Contents:

.. toctree::
   :maxdepth: 3

   modules


==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. Local Variables:
..  coding: utf-8
..  mode: rst
..  mode: flyspell
..  ispell-local-dictionary: "american"
..  fill-columnd: 90
.. End:
