#!/usr/bin/make -f
# -*- mode:makefile -*-

VERSION = $(shell python -c "import version; print(version.__version__)")

pypi-release:
	$(RM) -r dist
	python3 setup.py sdist
	twine upload dist/*
	version-summary

git-tag:
	git tag ${VERSION}
	git push --tags
	version-summary

git-rm-tag:
	git tag -d ${VERSION}
	git push --delete origin ${VERSION}

clean:
	$(RM) -r doc/_build build dist commodity.egg-info
