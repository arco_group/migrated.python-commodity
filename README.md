[![Documentation Status](https://readthedocs.org/projects/commodity/badge/?version=latest)](http://commodity.readthedocs.io/en/latest/?badge=latest)

**commodity** is a set of generic Python utility functions and clases (commodities) for
recurrent operations in almost any program.

[Read the docs](http://commodity.readthedocs.org/)
